# cmsscan

A unifying wrapper for CMS scan tools.
This script uses `droopescan` and `wpscan` to identify and scan CMS applications.

The script requires an input file with a list of domains that should be scanned.
Domains need to be separated by newline.

It first scans the domains using `droopescan` and tries to identify the exact
CMS. All identified wordpress instances are then additionally scanned with
`wpscan`. All Results are written as textfiles to the output directory, default
is `${HOME}/cmsscan_reports`.

## usage

    usage: cmsscan.sh <domain_list_file>
    
    Scan a list of domains for existing CMS and try to identify the specific CMS.
    If a wordpress instance is found, automatically scans with wpscan.
    
    Writes result reports as txt files to /home/tz/cmsscan_reports.
    